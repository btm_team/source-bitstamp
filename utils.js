'use strict'

module.exports = {
    round: function (float, digit) {
        const _digit = Math.pow(10, digit || 2);
        return Math.round(float * _digit) / _digit;
    },
    ceil: function (float, digit) {
        const _digit = Math.pow(10, digit || 2);
        return Math.ceil(float * _digit) / _digit;
    }
};