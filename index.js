'use strict';

const SourceAccessError = require('./errors/sourceAccessError');
const SourceError = require('./errors/sourceError');
const SourceConnectionError = require('./errors/sourceConnectionError');
const crypto = require('crypto');
const request = require('request-promise');
const utils = require('./utils');

class SourceBitstamp {
    constructor(authorizeData = {}) {
        this.url = 'https://www.bitstamp.net/api/';
        this.id = authorizeData.id || '';
        this.key = authorizeData.key || '';
        this.secret = authorizeData.secret || '';
        this.isSub = false;

        this.subKey = authorizeData.subKey || '';
        this.subSecret = authorizeData.subSecret || '';

        if (this.subKey.length && this.subSecret.length) {
            this.isSub = true;
        }

        this.support = {
            fiat: ['usd', 'eur'],
            crypto: ['btc', 'ltc', 'eth', 'xrp'],
            pairs: ['btcusd', 'btceur', 'xrpusd', 'xrpeur', 'ltcusd', 'ltceur', 'ethusd', 'etheur']
        };
        this.timeout = 5000;
        this.standardFee = 0.25;
    }

    deposit(currency) {
        currency = (currency || '').toLowerCase();
        if (this.support.crypto.indexOf(currency) === -1) 
            return Promise.reject(this._error(new Error(`Source not support currency "${currency}"`), 'deposit'));
        const currencyToUrl = {
            btc: 'bitcoin_deposit_address',
            ltc: 'v2/ltc_address',
            eth: 'v2/eth_address',
            xrp: 'v2/xrp_address'
        };

        return this._privateRequest('deposit', currencyToUrl[currency], {}, this.isSub);
    }

    rate(pair) {
        return this._publicRequest('rate', 'v2/ticker/', pair.toLowerCase().replace('_', ''))
            .then(rate => {
                if (typeof rate === 'object' && rate && rate.ask) return utils.round(rate.ask);
                throw new errors.ExchangeResponseError('Rate object parse error');
            });
    }

    balance(currency) { 
        currency = (currency || '').toLowerCase();
        if (this.support.fiat.indexOf(currency) === -1 && this.support.crypto.indexOf(currency) === -1)
            return Promise.reject(this._error(new Error(`Source not support currency "${currency}"`), 'balance'));
        return this.balances()
            .then(balances => {
                if (balances.hasOwnProperty(currency)) return balances[currency];
                throw new SourceResponseError(`Undefined currency ${currency}`);
            });
    }

    balances() {
        return this._privateRequest('balance', 'v2/balance', {}, this.isSub)
                .then(balances => {
                    return {
                        usd: utils.round(balances.usd_balance),
                        eur: utils.round(balances.eur_balance),
                        btc: utils.round(balances.btc_balance, 8)
                    }
                });
    }

    transactions() {
        return this._privateRequest('transactions', 'v2/user_transactions');
    }

    liquidationDeposit(currency) {
        return this._privateRequest('liquidation_deposit', 'v2/liquidation_address/new', {
            liquidation_currency: currency || 'usd'
        });
    }

    buy(pair, amount, extras = {}) {
        pair = (pair || '').toLowerCase().replace('_', '');

        if (this.support.pairs.indexOf(pair) === -1)
            return Promise.reject(this._error(new Error(`Source not support pair "${pair}"`), 'buy'));
        
        return this._privateRequest('buy','v2/buy/market/' + pair, {
            amount: amount
        }, this.isSub)
            .then(info => {
                const rate = parseFloat(info.price);
                const toAmount = parseFloat(info.amount);
                const fromAmount = utils.round(rate * toAmount);
                let fee = utils.round((fromAmount * ((extras.feePercent || this.standardFee) / 100)) / rate, 8);
                return {
                    id: info.id,
                    status: 'finished',
                    fee: fee,
                    rate: rate,
                    fromAmount: utils.round(fromAmount + fee),
                    fromCurrency: pair.substr(3),
                    toAmount: toAmount,
                    toCurrency: pair.substr(0, 3)
                };
            });
    }

    buyStatus(id) {
        return this._privateRequest('checkBuyStatus', 'order_status', {
            id: parseInt(id)
        }, this.isSub);
    }

    _preSend(currency, amount) {
        if (!this.isSub) return Promise.resolve();

        return this._privateRequest('transfer', 'v2/transfer-to-main/', {
            amount: amount,
            currency: currency.toUpperCase()
        }, this.isSub);
    }

    send(currency, address, amount, fee) {
        currency = (currency || '').toLowerCase();
        if (this.support.crypto.indexOf(currency) === -1) 
            return Promise.reject(this._error(new Error(`Source not support currency "${currency}"`), 'send'));

        const currencyToUrl = {
            btc: 'bitcoin_withdrawal',
            ltc: 'v2/ltc_withdrawal',
            eth: 'v2/eth_withdrawal',
            xrp: 'ripple_withdrawal'
        };

        return this._preSend(currency, amount)
            .then(() => {
                return this._privateRequest('send', currencyToUrl[currency], {
                    amount: amount,
                    address: address
                });
            })
            .then(info => {
                return {
                    id: info.id.toString(),
                    status: 'open'
                };
            });
    }

    sendStatus(id) {
        const status = {
            '0': 'open',
            '1': 'in_process',
            '2': 'finished',
            '3': 'canceled',
            '4': 'failed'
        };
        return this._privateRequest('checkSendStatus','withdrawal_requests')
            .then(list => {
                id = parseInt(id);
                for (let i = 0; i < list.length; i++) {
                    if (list[i].id === id) {
                        return {
                            id: id.toString(),
                            status: status[list[i].status],
                            txId: list[i].transaction_id || ''
                        };
                    }
                }
            });
    }

    _publicRequest(methodName, customMethod, param) {
        let url = [customMethod, '/'];
        if (param) url.push(param + '/');
        const options = {
            url: this.url + url.join(''),
            json: true,
            timeout: this.timeout
        };
        return request(options)
            .catch(err => this._error(err, methodName));
    }

    _privateRequest(methodName, customMethod, data = {}, isSub) {
        const sourceParams = { key: this.key, secret: this.secret };
        if (typeof isSub !== 'undefined' && isSub) {
            sourceParams.key = this.subKey;
            sourceParams.secret = this.subSecret;
        }
        const nonce = Date.now().toString();
        const update = [nonce, this.id, sourceParams.key].join('');
        const signature = crypto.createHmac('sha256', Buffer.from(sourceParams.secret, 'utf8')).update(update).digest('hex').toUpperCase();

        data.key = sourceParams.key;
        data.signature = signature;
        data.nonce = nonce;

        const options = {
            url: `${this.url}${customMethod}/`,
            method: 'POST',
            json: true,
            form: data,
            timeout: this.timeout
        };

        return request(options)
            .then(body => {
                if (body && body.status === 'error' && body.reason) {
                    throw new Error(JSON.stringify(body.reason));
                }
                if (body && body.error) {
                    if (typeof body.error === 'string') throw new Error(body.error);
                    throw new Error(JSON.stringify(body.error));
                }
                return body;
            })
            .catch(err => { throw this._error(err, methodName)});
    }

    _error(err, method) {
        const normalize = message => message.replace('{"__all__":["', '').replace('{"amount":["', '').replace('"]}', '')
        const message = err.message.toLowerCase();
        if (message.indexOf('api key not found') !== -1) throw new SourceAccessError('bitstamp', method, err.message, 'ERR_SOURCE_ACCESS_KEY');
        if (message.indexOf('invalid signature') !== -1) throw new SourceAccessError('bitstamp', method, err.message, 'ERR_SOURCE_ACCESS_SIGN');
        if (message.indexOf('no permission found') !== -1) throw new SourceAccessError('bitstamp', method, err.message, 'ERR_SOURCE_ACCESS_PERMITION');
        if (message.indexOf('not allowed to withdraw to specified bitcoin address') !== -1) throw new SourceAccessError('bitstamp', method, err.message, 'ERR_SOURCE_ACCESS_ADDRESS');
        if (message.indexOf('you can only buy') !== -1) throw new SourceError('bitstamp', method, normalize(err.message), 'ERR_SOURCE_INSUFFICIENT_FUNDS_TO_BUY');
        if (message.indexOf('you have only') !== -1) throw new SourceError('bitstamp', method, normalize(err.message), 'ERR_SOURCE_INSUFFICIENT_FUNDS_TO_SEND');
        if (message.indexOf('minimum order size') !== -1) throw new SourceError('bitstamp', method, normalize(err.message), 'ERR_SOURCE_LIMIT_BUY');
        if (message.indexOf('ensure this value is greater') !== -1) throw new SourceError('bitstamp', method, normalize(err.message), 'ERR_SOURCE_LIMIT_SEND');
        if (message.indexOf('getaddrinfo enoet www.bitstamp.net:443') !== -1) throw new SourceConnectionError('bitstamp', method, err.message, 'ERR_SOURCE_CONNECTION');
        if (message.indexOf('source not support') !== -1) throw new SourceError('bitstamp', method, err.message, 'ERR_SOURCE_NOT_SUPPORT_CURRENCY');
        return new SourceError('bitstamp', method, err.message, 'ERR_SOURCE_ERROR');
    }
}

module.exports = {
    type: 'exchange',
    source: SourceBitstamp,
    errors: { SourceError, SourceAccessError, SourceConnectionError }
};