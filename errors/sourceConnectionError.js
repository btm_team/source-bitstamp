'use strict';

const SourceError = require('./sourceError');

class SourceConnectionError extends SourceError {
    constructor(exchangeName, methodName, message, code) {
        super(exchangeName, methodName, message, code);
        this.name = 'SourceConnectionError';

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, SourceConnectionError);
        } else {
            this.stack = (new Error()).stack;
        }
    }
}

module.exports = SourceConnectionError;