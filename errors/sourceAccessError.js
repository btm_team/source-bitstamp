'use strict';

const SourceError = require('./sourceError');

class SourceAccessError extends SourceError {
    constructor(exchangeName, methodName, message, code) {
        super(exchangeName, methodName, message, code);
        this.name = 'SourceAccessError';

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, SourceAccessError);
        } else {
            this.stack = (new Error()).stack;
        }
    }
}

module.exports = SourceAccessError;